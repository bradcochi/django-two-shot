from django.urls import path
from receipts.views import (
    receipt_list,
    create_receipt,
    receipts_categories,
    receipts_accounts,
    create_expense_category,
    create_account,
)


urlpatterns = [
    path("accounts/create", create_account, name="create_account"),
    path(
        "categories/create",
        create_expense_category,
        name="create_expense_category",
    ),
    path("accounts/", receipts_accounts, name="receipts_accounts"),
    path("categories/", receipts_categories, name="receipts_categories"),
    path("create/", create_receipt, name="create_receipt"),
    path("", receipt_list, name="home"),
]
